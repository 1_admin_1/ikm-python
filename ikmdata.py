from tkinter import *
from tkinter.filedialog import askopenfilenames
from tkinter.messagebox import showerror
import matplotlib.pyplot as plt
import time
import numpy as np
import gc
import datetime
import matplotlib.ticker as ticker
import matplotlib

s_freq=500000
s_channel=6
s_filebegin=9
#xz=np.arange(1,100000000)
memoryAllFile=False

class MainWindow(Frame):
	"""
	Open binary files written IKMADC
	Read files, plot preview and original data.
	use self.data.channeldata[num_channel] in inherited class for get channel adc data

	"""
	def __init__(self):
		Frame.__init__(self)
		self.openfile=OpenFileFrame()
		self.openfile.grid()
		Button(self,command=self.generateData,text="Generate Data").grid(columnspan=2)
		self.chart=[]
		self.colon2=IntVar()
		Checkbutton(self, text="2-colon", variable=self.colon2).grid(column=1)
		self.channelEntry=Entry(self,width=10)
		self.labelEntry("Channels:",self.channelEntry)
		self.scaleEntry=Entry(self,width=10)
		self.labelEntry("Time (s):",self.scaleEntry)
		Button(self,command=self.scale,text="Scale").grid(columnspan=2)
		Button(self,command=self.savePlot,text="Save Plot").grid(columnspan=2)
		self.data=Data(self)
	def windowcharts(self):
		plt.ion()
		col=2 if self.colon2.get() else 1
		row=int(round(len(self.getChannel())/col))
		self.fig, ax = plt.subplots(row,col, sharex=True)
		self.fig.subplots_adjust(hspace=0,wspace=0.15,bottom=0.04,left=0.1,top=0.99,right=0.98)
		arr=np.array(ax)
		self.chart=arr.reshape(arr.size,order='F')
		def on_draw(event):
			cor=self.fig.get_size_inches()
			y=cor[1]
			self.fig.subplots_adjust(hspace=0,wspace=0.8/y,bottom=0.4/y,left=0.6/y,right=1-(0.1/y),top=0.99)
			return True
		#self.fig.canvas.mpl_connect('draw_event', on_draw)
	def scale(self):
		self.windowcharts()
		self.drawchart(self.data.chunk(self.getMinMax()))
		gc.collect()
	def drawchart(self,arr,datax=None,diapason=None,):
		self.begin=self.getMinMax()[0]
		for a,i in zip(arr, self.chart):
			if datax is not None:
				i.plot(datax,a)
				i.xaxis.set_major_formatter(ticker.FuncFormatter(self.format_signalsec))
			else:
				i.plot(a)
				i.xaxis.set_major_formatter(ticker.FuncFormatter(self.format_signaltick))
			#self.fig.autofmt_xdate()
		self.fig.canvas.draw()
	def savePlot(self):
		savePNG()
	def format_signaltick(self,x, pos=None):
		return (datetime.timedelta(seconds=((x+self.begin)/s_freq))+self.openfile.time).strftime('%H:%M:%S,%f')[:-3]
	def format_signalsec(self,x, pos=None):
		return (datetime.timedelta(seconds=x)+self.openfile.time).strftime('%H:%M:%S')
	def generateData(self):
		self.data.generateData(self.openfile.data,self.getMinMax())
		if not memoryAllFile: self.openfile.erase()
		self.windowcharts()
		self.drawchart(self.data.getPreview(),datax=self.data.timepreview)
		if not self.scaleEntry.get():
			self.scaleEntry.insert(END,"0-%i"%(self.data.size/s_freq))
		gc.collect()
		f=open("tempNoFilterData.bin",'wb')
		np.save(f,self.data.channeldata[1][1590757:26449651])
		f.close()
	def getChannel(self):
		strChannel=self.channelEntry.get()
		return range(1,s_channel+1) if not strChannel else rangeInt(strChannel)

	def getMinMax(self):
		if self.scaleEntry.get():
			findcolon=len(self.scaleEntry.get().split(":"))-1
			if findcolon:
				if findcolon==2:
					format="%H:%M"
					tstart,tend=self.scaleEntry.get().split("-")
				else:	
					format="%H:%M:%S"
					tstart,tend=self.scaleEntry.get().split("-")
				timefile=self.openfile.time
				tstart,tend=self.scaleEntry.get().split("-")
				timefile=self.openfile.time
				mn=datetime.datetime.strptime(tstart, format)-timefile
				mx=datetime.datetime.strptime(tend, format)-timefile
				mn=int(mn.total_seconds()*s_freq); mx=int(mx.total_seconds()*s_freq);
				print(mn,mx)
				return (mn,mx)
			else:
				minSec,maxSec=self.scaleEntry.get().split("-")
				mn=int(float(minSec)*s_freq); mx=int(float(maxSec)*s_freq);
				print(mn,mx)
			return (mn,mx)
		else:
			return (0,-1)

	def labelEntry(self,text,entry):
		row=self.grid_size()[1]
		Label(self,text=text).grid(row=row,column=0)
		entry.grid(row=row,column=1)
	def getRow(self):
		return self.grid_size()[1]


class Data():
	def __init__(self,window):
		self.arrEmpty()
		self.window=window
	def generateData(self,data,diapason):
		global s_channel
		s_channel=data[4]
		#print(data[0:10])
		begin,end=diapason
		self.arrEmpty()
		size=(data.size-10)/s_channel if end==-1 else end-begin
		self.size=size
		previewstep=int(size/39475)
		arrbegin=s_filebegin+begin*s_channel
		arrend = data.size if end==-1 else s_filebegin+end*s_channel
		for i in self.getChannel():
			chdata=data[arrbegin+i:arrend:s_channel]
			chpreview=data[arrbegin+i:arrend:s_channel*previewstep]
			if not memoryAllFile: 
				chdata=np.array(chdata)
				chpreview=np.array(chpreview)
			self.channeldata[i]=chdata
			self.channelpreview[i]=(chpreview)
		self.timepreview=np.linspace(0,size/s_freq,num=len(chpreview))
	def arrEmpty(self):
		self.channeldata={}
		self.channelpreview={}
		self.timepreview=[]

	def chunk(self, diapason):
		begin,end=diapason
		arr=[]
		for ch in self.getChannel():
			arrend= len(self.channeldata[ch]) if end==-1 else end
			arr.append(self.channeldata[ch][begin:arrend])
		return arr

	def getPreview(self):
		return [self.channelpreview[i] for i in self.getChannel()]

	def getChannel(self):
		return self.window.getChannel()

class OpenFileFrame(Frame):
	def __init__(self,parent=None,text="Open File"):
		Frame.__init__(self,parent)
		self.button = Button(self, text=text, command=self.load_file, width=10)
		self.button.pack()
		self.parent=parent
	def load_file(self):
		for i in self.load_file_iter():
			pass
	def load_file_iter(self):
		filenames=askopenfilenames(filetypes=(("bin data files", "*.bin"),))
		if type(filenames)==str:
			filenames=filenames.split(".bin ")
			filenames=[i if i[-4:]=='.bin' else i+'.bin' for i in filenames]
		print(filenames)
		for fn in filenames:
			if fn:
				self.read_file(fn)
				self.name_file=fn.split("/")[-1].replace('.bin','')
				yield self.data

	def read_file(self,fname):
		timeStart=time.time()
		self.data=np.fromfile(fname,dtype=np.dtype(np.int16),count=-1)
		format = '%H-%M-%S'
		self.time=datetime.datetime.strptime(fname.split("_")[-1][:-4], format)
		self.fname=fname
		print(time.time()-timeStart)

	def erase(self):
		self.data=[]


def view(frame):
	root=Tk()
	fr=frame()
	fr.grid()
	fr.root=root
	root.mainloop()

def savePNG(A4=False):
	import os.path
	for i in range(1000):
		name="chart%03d"%i
		if not os.path.isfile(name+'.png'):
			print(name)
			break
	fig = plt.gcf()
	ax=plt.gca()
	target=[[5,3,''],[3,3,'-colon'],[6,4,'-big']]
	maximum=-10000
	minimum=10000
	plt.xticks(rotation=20)
	if A4:
		target.append([11,7,'-A4'])
	for w,h,ext in target:
		fig.set_size_inches(w,h)
		for line in ax.get_lines():
			yd = line.get_ydata()
			maximum=max(yd) if max(yd)>maximum else maximum
			minimum=min(yd) if min(yd)<minimum else minimum
		crop1=(maximum-minimum)*0.05+maximum
		crop2=minimum-(maximum-minimum)*0.05
		ax.set_ylim([crop2,crop1])
		if h>5: plt.xticks(rotation=0)
		fig.savefig(name+ext+'.png',dpi=300, bbox_inches='tight')
	plt.xticks(rotation=0)

def changeSetting(setting):
	for key in setting:
		globals()[key] = setting[key]

def rangeInt(s):
	return sum(((list(range(*[int(j) + k for k,j in enumerate(i.split('-'))]))
		if '-' in i else [int(i)]) for i in s.split(',')), [])

if __name__=="__main__":
	view(MainWindow)

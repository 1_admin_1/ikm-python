#Author: 1_admin_1


from ikmdata import *
from multiprocessing import Pool,cpu_count
import matplotlib.ticker as ticker
import datetime
#---
c_levelMaxMin=100000;
c_deltaSignal=4000;
c_numMarks=26
c_fi=100000
c_parallelsec=4
cpu=cpu_count()
#---
#matplotlib.rcParams.update({'font.size': 22})
class SignalProcessingWindow(MainWindow):
	"""
	Window for manage processing and analyze singal 
	Uses ikmdata for read and view signal in different channels ADC

	"""
	def __init__(self):
		MainWindow.__init__(self)
		Button(self,text="Analyze Signal",command=lambda:self.work()).grid(columnspan=2)
		Button(self,text="Cache",command=lambda:self.writeCache()).grid(column=0)
		Button(self,text="Use Cache",command=lambda:self.readCache()).grid(column=1,row=self.getRow()-1)
		self.plotInLastChart=IntVar()
		self.printPlotData=IntVar()
		Checkbutton(self,variable=self.plotInLastChart,text='Plot in Last Chart').grid(columnspan=2)
		Checkbutton(self,variable=self.printPlotData,text='Print Plot Data').grid(columnspan=2)
		Button(self,text='Diff',command=self.difference).grid(column=0)
		Button(self,text='DiffTick',command=self.differenceTick).grid(column=1,row=self.getRow()-1)
		Button(self,text='Hist',command=self.hist).grid(columnspan=2)
		Button(self,text="RPM",command=lambda:self.RPM()).grid(columnspan=2)
		Button(self,text="Torsion",command=lambda:self.torsion()).grid(columnspan=2)
		self.entrytrend=Entry(self,width=8)
		self.entrytrend.grid()
		Button(self,text="Trend",command=lambda:self.trend()).grid(column=1,row=self.getRow()-1)
		self.correctionMark=Entry(self,width=8)
		self.correctionMark.grid(column=0)
		Button(self,text="FixMark",command=lambda:self.fixMark()).grid(column=1,row=self.getRow()-1)
		self.angleAnalyse=IntVar()
		Checkbutton(self,variable=self.angleAnalyse,text='Angle Analyse').grid(columnspan=2)
		self.signal={}
		self.listbox = []
		self.chartdata=[]
		r=self.getRow()
		for e,i in enumerate(["Channel A", "Channel B"]):
			fr=Frame(self)
			Label(fr,text=i).pack()
			self.listbox.append(Listbox(fr,width=8,exportselection=0))
			self.listbox[-1].pack()
			fr.grid(row=r,column=e)
	def work(self):
		for e,i in enumerate(self.getChannel()):
			self.signal[i]=Signal(self.data.channeldata[i])
			s=self.signal[i]
			s.processing(self.getMinMax(),self.angleAnalyse)
			s.gettime()
			s.analyse()
			if True:
				plt.figure()
				plt.plot([i-j for i,j in zip(s.time,s.time[1:])])
				plt.show()
			#print(s.time[:50])
			if e<2: self.updateMarks(e,i)
	def updateMarks(self,e,i):
			self.listbox[e].delete(0, END)
			for j in self.signal[i].goodMarks:
				self.listbox[e].insert(END,str(j))
				self.listbox[e].update()
			print("Marks:",self.signal[i].numMarks)
	def RPM(self):
		print('efef')
		j=self.getChannel()[0]
		self.plot(self.signal[j].getRPM(int(self.listbox[0].get(ACTIVE))))
	def torsion(self):
		tempTime=[]
		for e,i in enumerate(self.getChannel()):
			s=self.signal[i]
			m1=int(self.listbox[e].get(ACTIVE))
			tempTime.append([[i,d2-d1] for i,d1,d2 in zip(s.time[m1::s.numMarks],s.time[m1+s.numMarks::s.numMarks],s.time[m1+s.numMarks*2::s.numMarks])])
		d=[]
		t=[]
		for [i1,j1],[i2,j2] in zip(*tempTime):
			d.append(360*2*(i1-i2)/(j1+j2))
			t.append((i1+i2)/2)
		self.plot((t,d))
	def plot(self,data,ignore=False):
		if not (self.plotInLastChart.get()) or ignore:
			self.fig1, self.ax1 = plt.subplots(figsize=(16,8))
			self.fig1.subplots_adjust(bottom=0.08,top=0.97,left=0.08,right=0.93)
			self.chartdata=[]
		if self.printPlotData.get():
			if isinstance(data, tuple):
				self.root.clipboard_clear()
				self.root.clipboard_append(data[1])

		mn,mx=self.getMinMax()
		#print(mx,s_freq,len(data))
		if isinstance(data, tuple):
			mintick,maxtick=self.getMinMax()
			mintick*=c_fi; maxtick*=c_fi;
			data=[(i,j)for i,j in zip(*data) if mintick<i<maxtick]
			data=list(zip(*data))
			self.chartdata.append(data)
			self.ax1.xaxis.set_major_formatter(ticker.FuncFormatter(self.format_datetick))
			plt.plot(data[0][:len(data[1])],data[1])
		else:
			self.ax1.xaxis.set_major_formatter(ticker.FuncFormatter(self.format_date))
			plt.plot(np.linspace(mn/s_freq,(mx/s_freq),num=len(data)),data)
		#fig.autofmt_xdate()
		plt.grid(True)
		plt.locator_params(nbins=6)
		plt.draw()
	def difference(self):
		diff=[i-j for i,j in zip(self.chartdata[0][1],self.chartdata[1][1])]
		self.plot((self.chartdata[0][0][:len(diff)],diff,),ignore=True)
	def differenceTick(self):
		diff=[60*(1/i-1/(2*i-j))*500000 for i,j in zip(self.chartdata[0][1],self.chartdata[1][1])]
		self.plot((self.chartdata[0][0][:len(diff)],diff,),ignore=True)
	def hist(self):
		plt.figure()
		dt=self.chartdata[0][1]
		plt.hist(dt,20)
		print("Median:",np.median(dt),"Std:",np.std(dt))
		plt.draw()
	def trend(self):
		tempTime=[]
		for e,i in enumerate(self.getChannel()):
			s=self.signal[i]
			m1=int(self.listbox[e].get(ACTIVE))
			tempTime.append([[i,d2-d1] for i,d1,d2 in zip(s.time[m1::s.numMarks],s.time[m1+s.numMarks::s.numMarks],s.time[m1+s.numMarks*2::s.numMarks])])
			#tempTime.append([[i,d2-d1] for i,d1,d2 in zip(s.time[m2::s.numMarks],s.time[m1::s.numMarks],s.time[m1+s.numMarks::s.numMarks])])
		d=[]
		for [i1,j1],[i2,j2] in zip(*tempTime):
			d.append(360*2*(i1-i2)/(j1+j2))
		dsize=len(d)
		num=int(self.entrytrend.get())
		arr=[sum(d[i:i+num])/num for i in range(dsize-num)]
		self.plot(arr)
	def format_date(self,x, pos=None):
		return (datetime.timedelta(seconds=x)+self.openfile.time).strftime('%H:%M:%S,%f')[:-4]
	def format_datetick(self,x, pos=None):
		return (datetime.timedelta(seconds=x/c_fi/s_freq)+self.openfile.time).strftime('%H:%M:%S,%f')[:-4]
	def fixMark(self):
		self.signal[self.getChannel()[0]].fixMark(int(self.correctionMark.get()))
		self.updateMarks(0,self.getChannel()[0])

	def writeCache(self):
		for i in self.getChannel():
			fname=self.openfile.fname.replace('.bin','@'+str(i)+'.cache')
			self.signal[i].writeCache(fname)
		print("Cache Ok")
	def readCache(self):
		for e,i in enumerate(self.getChannel()):
			self.signal[i]=Signal(self.data.channeldata[i])
			fname=self.openfile.fname.replace('.bin','@'+str(i)+'.cache')
			self.signal[i].readCache(fname)
			if e<2: self.updateMarks(e,i)


class Signal():
	"""
	Base algoritm processing signal
	Calculate precision passing mark time (using processing linear parts)
	parallel processing
	cache for save/load calc result.
	"""
	def __init__(self, data, percent=0.4,title=''):
		self.data=data
		self.size=data.size
		self.percent=percent
		self.title=title
		self.time=[]
		self.angle={}
		self.globmax=0
		self.globmin=0
		self.useCache=False
	def processing(self,minmax,angleAnalyse):
		self.angleAnalyse=angleAnalyse
		mind,maxd=minmax
		if maxd==-1: maxd=self.size-mind
		diapason=maxd-mind
		self.linetime=[[],[]]
		if diapason<2*c_parallelsec*s_freq:
			self.linetime=self.processingChunk(minmax)
		else:
			psec=int(diapason/cpu)
			args=[]
			for i in range(cpu):
				maxlocal=(i+1)*psec+50000+mind
				minlocal=i*psec+mind
				if maxlocal<maxd:
					args.append((minlocal,maxlocal))
				else:
					args.append((minlocal,maxd))
			#print(args)
			pool = Pool()
			results = pool.map(self.processingChunk, args)
			pool.close()
			pool.join()
			for i in range(len(results)):
				for j in [0,1]:
					arr=results[i][j]
					if self.linetime[j]:
						endr=self.linetime[j][-1]
						r=arr.index(endr)+1
					else:
						r=0
					self.linetime[j].extend(arr[r:])
			#pool.close()
	def processingChunk(self,startEnd):
		print('startend',startEnd)
		sys.stdout.flush()
		start,end=startEnd
		linetime=[[],[]]
		maxlocal=-c_levelMaxMin;	minlocal=c_levelMaxMin
		maxnum=minnum=index=d1=d2=0
		maxtime=mintime=linesarray=[]
		try:
			firstMark=0
			index=0
			for i,d in zip(range(start,end),self.data[start:end]):
				if d>maxlocal and d1-d2>d-d1:
					maxlocal=d; index=i; maxnum+=1
				if d<minlocal and d1-d<d2-d1:
					minlocal=d; index=i; minnum+=1
				d2=d1
				d1=d
				if maxnum>minnum and maxlocal-d>c_deltaSignal:
					line=[]
					cropmin=minlocal+(maxlocal-minlocal)*self.percent
					cropmax=maxlocal-(maxlocal-minlocal)*self.percent#*1.75
					minnum=maxnum=0
					while(self.data[index]>cropmax): index-=1
					while(self.data[index]>cropmin): 
						if self.data[index]>cropmax:
							line=[]
						else:
							line.append(self.data[index]) 
						index-=1
					minlocal=c_levelMaxMin
					if firstMark>2:
						linetime[0].append(self.linetotime(index,line, cropmax, cropmin))
					else:
						firstMark+=1
				if minnum>maxnum and d-minlocal>c_deltaSignal:
					line=[]
					cropmin=minlocal+(maxlocal-minlocal)*self.percent
					cropmax=maxlocal-(maxlocal-minlocal)*self.percent#*1.75
					minnum=maxnum=0
					while(self.data[index]<cropmin): index-=1;
					while(self.data[index]<cropmax): 
						if self.data[index]<cropmin:
							line=[]
						else:
							line.append(self.data[index])
						index-=1
					maxlocal=-c_levelMaxMin
					if firstMark>2:
						linetime[1].append(self.linetotime(index,line, cropmax, cropmin))

					else:
						firstMark+=1
			linetime[0].pop(0)
			linetime[1].pop(0)
			return linetime
		except:
			#print(linetime)
			print(sys.exc_info()[0])
			print("Error on time:",index)

	def approximate(self,line):
		x = np.arange(0,len(line))
		y = np.array(line[::-1])
		A = np.vstack([x, np.ones(len(x))]).T
		m, c = np.linalg.lstsq(A, y)[0]
		return m,c

	def approximate1(self,line):
		n=0;
		sx=0; sx2=0; sy=0; sxy=0;
		dtime=0;
		for dtime,dvalue in enumerate(line[::-1]):
			sx+=dtime;
			sy+=dvalue;
			sx2+=dtime*dtime;
			sxy+=dtime*dvalue;
			n+=1
		D = n * sx2 - sx * sx;
		b = (sy * sx2 - sx * sxy)/D;
		a = (n * sxy - sx * sy)/D; 
		#print(sx,sx2,sy,sxy,D)
		return a,b


	def linetotime(self,index,line, cropmax, cropmin):
		if(len(line)<10):
			print('Error with index',index)
			return 0
		#print([index,line])
		m,c=self.approximate(line)
		#print(m,c)
		t=(((cropmax+cropmin)/2)-c)/m
		if self.angleAnalyse:
			self.angle[index*c_fi+int(c_fi*t)]=m;
		return index*c_fi+int(c_fi*t)

	def gettime(self):
		if self.linetime[0][0]<self.linetime[1][0]:
			self.linetime[0].pop(0)

		self.time=[int((i+j)/2) for i,j in zip(*self.linetime)][1:]
	def analyse(self):
		self.inversetime=[i+j for i,j in zip(self.linetime[0][:2*c_numMarks+1],self.linetime[1][1:2*c_numMarks+2])]
		self.inversetime=[int((i-j)/2) for i,j in zip(self.inversetime[1:],self.inversetime)]
		x=self.getAKF(self.inversetime)
		self.numMarks=x.index(max(x))+1
		mean=np.mean(self.inversetime)

		self.badMarks=[i for i,j in enumerate(self.inversetime) if abs(j-mean)/mean>0.2]
		self.badMarks=[i for i in self.badMarks if i<self.numMarks]
		self.goodMarks=[i for i in range(self.numMarks) if i not in self.badMarks]
	def getAKF(self,x):
		return [np.corrcoef(x[:-i], x[i:])[0][1] for i in range(1, c_numMarks+1)]
	def getTimeTurn(self,x):
		self.TimeTurn=[j-i for i,j in zip(self.time[x::self.numMarks],self.time[x+self.numMarks::self.numMarks])]
	def getRPM(self,x):
		print(self.angle)
		self.getTimeTurn(x)
		return (self.time[x::self.numMarks],[500000*c_fi*60*1/i for i in self.TimeTurn])
	def fixMark(self,correct):
		yyy=[self.getAKF(self.time[i:i+60]) for i in range(len(self.time)-60)]
		self.realMark=[i.index(max(i)) for i in yyy]
		dd=[e for e,(j1,j2) in enumerate(zip(self.realMark,self.realMark[1:])) if j1!=j2]
		begin=0
		for i in dd:
			if self.realMark[i:i+3]==[0,self.numMarks-2,self.numMarks-2]:
				begin=i
				break
		if dd:
			badmark=[(begin+i-1)%self.numMarks for i in range(0,4)]
			self.goodMarks=[i for i in self.goodMarks if i not in badmark]
			for j in np.arange(begin+correct*self.numMarks,0,-self.numMarks):
				del self.time[j]
			self.numMarks-=1
	def readCache(self,fname):
		f = open(fname,"rb")
		self.time=np.load(f)
		self.goodMarks=np.load(f)
		self.numMarks=np.load(f)
		self.useCache=True
		f.close()
	def writeCache(self,fname):
		f = open(fname,"wb")
		np.save(f,self.time)
		np.save(f,self.goodMarks)
		np.save(f,self.numMarks)
		f.close()


if __name__=="__main__":
	view(SignalProcessingWindow)

import math
import time
from timeMarksFile import timeMarks
from matplotlib import pyplot as plt
M_RAD=toRad=math.pi/180
M_ANG=toAng=1/toRad
M_PI=math.pi
#timeMarks=[[]]*3
numMarks=16
halfRev=int(numMarks/2)
numBigMark=0
numMarksBetweenSensor=2
angleSensors=[45.0,45.0]
arrX=None
arrY=None
radius=215/2
realX=2
realY=2
x=y=0
angleMarks=[x/numMarks*360 for x in range(numMarks)]
timeMarks[0]=timeMarks[0][:33]
timeMarks[1]=timeMarks[1][:33]
timeMarks[2]=timeMarks[2][:33]
print ("angleMarks: ",angleMarks)
A1=-math.sin(toRad*angleSensors[0])
A2=math.sin(toRad*angleSensors[1])
B1=math.cos(toRad*angleSensors[0])
B2=math.cos(toRad*angleSensors[1])
phi12=0
phi23=0

angle=[]
angle1=[]
angle2=[]
angle3=[]
angleOrigTemp=[]
angleOrig=[]
angleResult=[]

print(A1,A2)
fig, ax = plt.subplots()
points, = ax.plot(0, 0, marker='o')

def errorFindBeats(x,y):
	return (math.asin((y*B2-x*A2)/radius) - math.asin(y/radius)-phi23)**2+(math.asin(y/radius)-math.asin((y*B1-x*A1)/radius)-phi12)**2


def FindBeatsApprx():
	y=radius*(phi12/A1+phi23/A2)/((B2-1)/(A2)-(B1-1)/A1)
	x=(phi12*radius+(y)*B1-y)/A1
	return x,y

def FindBeatsPrecision():
	global x,y
	step=0.01
	errr=errorFindBeats(x,y)
	while(errr>(10^(-20))):
		while(errr>errorFindBeats(x,y+step)):
			y+=step;
			errr=errorFindBeats(x,y);
		while(errr>errorFindBeats(x,y-step)):
			y-=step;
			errr=errorFindBeats(x,y);
		while(errr>errorFindBeats(x+step,y)):
			x+=step;
			errr=errorFindBeats(x,y);
		while(errr>errorFindBeats(x-step,y)):
			x-=step;
			errr=errorFindBeats(x,y);
		if(step<0.00000000000000001): 
			#print("err: ",errr)
			break;
		step=step/10;

def FindBeats():
	global x,y,phi23,phi12
	size=min(len(timeMarks[1]),len(timeMarks[2]),len(timeMarks[0]))
	arrY=[]
	arrX=[]
	for i in range(halfRev,size-halfRev):
		iii=numMarks+i-numBigMark;
		phiM=M_RAD*(angleMarks[(iii+numMarksBetweenSensor)%numMarks]-
			angleMarks[(iii)%numMarks]);
		if(phiM<0): phiM+=2*M_PI;
		phi12=(M_RAD*angleSensors[0]+phiMarks(0,1,i,i)-phiM);
		phiM=M_RAD*(angleMarks[(iii)%numMarks]-
			angleMarks[(iii-numMarksBetweenSensor)%numMarks]);
		if(phiM<0): phiM+=2*M_PI;
		phi23=(M_RAD*angleSensors[1]+phiMarks(1,2,i,i)-phiM);
		#print("phi: ",toAng*phi12,toAng*phi23)
		x,y=FindBeatsApprx();
		print("x,y: ",x,y)
		FindBeatsPrecision();
		print("x1,y: ",x,y)
		arrY.append(y);
		arrX.append(x);
	print (arrX,arrY)

def phiMarks(ch1,ch2,numMark1,numMark2):
	Trev=(timeMarks[ch1][numMark1+halfRev])-(timeMarks[ch1][numMark1-halfRev]);
	Trev+=(timeMarks[ch2][numMark2+halfRev])-(timeMarks[ch2][numMark2-halfRev]);
	Trev=Trev/2;
	phiTime=2*M_PI*(timeMarks[ch1][numMark2]-timeMarks[ch2][numMark1]);
	return phiTime/Trev;

def findAngle():
	global x,y,phi23,phi12, angleResult, angle, angleOrig
	size=min(len(timeMarks[1]),len(timeMarks[2]),len(timeMarks[0]))
	size=size-size%numMarks;
	for i in range(numMarks):
		angle.append(0)
		angleOrigTemp.append(0)
		angleResult.append(0)
		angleOrig.append(0)
	for i in range(halfRev,size-halfRev):
		angle[(numMarks+i+numMarksBetweenSensor)%numMarks]+=phiMarks(0,0,i,i+1)
		print(phiMarks(0,0,i,i+1))
		angle[(numMarks+i)%numMarks]+=phiMarks(1,1,i,i+1)
		angle[(numMarks+i-numMarksBetweenSensor)%numMarks]+=phiMarks(2,2,i,i+1)
		angleOrigTemp[(numMarks+i)%numMarks]+=phiMarks(1,1,i,i+1)
	numCalc=size/numMarks-1
	print(angle)
	for i in range(1,numMarks):
		angleResult[i]=angleResult[i-1]+toAng*angle[i-1]/(3*numCalc)
		angleOrig[i]=angleOrig[i-1]+toAng*angleOrigTemp[i-1]/numCalc
		angle[i-1]=0
	print(angleOrig)
	angle[-1]=0
	#angleResult=angleOrig.copy()
	#angleOrig=angleResult.copy()
	angleResult=[x/numMarks*360 for x in range(numMarks)]
	prevAngle=[0 for x in range(numMarks)]
	ax.set_xlim(-2,2)
	ax.set_ylim(-2,2)
	print("orig",angleOrig)
	while 1:
		xx=[]
		yy=[]
		time.sleep(1)
		for i in range(halfRev,size-halfRev):
			iii=numMarks+i-numBigMark;
			phiM=M_RAD*(angleResult[(iii+numMarksBetweenSensor)%numMarks]-
				angleResult[(iii)%numMarks]);
			if(phiM<0): phiM+=2*M_PI;
			phi12=(M_RAD*angleSensors[0]+phiMarks(0,1,i,i)-phiM);
			phiM=M_RAD*(angleResult[(iii)%numMarks]-
				angleResult[(iii-numMarksBetweenSensor)%numMarks]);
			if(phiM<0): phiM+=2*M_PI;
			phi23=(M_RAD*angleSensors[1]+phiMarks(1,2,i,i)-phiM);
			#print("phi: ",toAng*phi12,toAng*phi23)
			x,y=FindBeatsApprx();
			FindBeatsPrecision();
			#print("x,y: ",x,y)
			xx.append(x)
			yy.append(y)
			angle[(i)%numMarks]+=math.asin(y/radius)
		points.set_data(xx,yy)
		plt.pause(1)
		print('angle', [toAng*a for a in angle])
		print('angleResult', angleResult)
		print("numCalc",numCalc)
		print('angleOrig',angleOrig)
		for i in range(16):
			print(i,angleResult[i+1]-toAng*angle[i])
		for i in range(numMarks):
			#print(toAng*angle[i]/numCalc)
			#print(angle[i]-prevAngle[i])
			angleResult[i]=angleResult[i]-toAng*(angle[(i)%numMarks])/(numCalc)
			if i!=0:
				angleResult[i]=angleResult[i]-angleResult[0]
		prevAngle=angle
		print(angleResult)
		angle=[0 for i in range(numMarks)]
		angleResult[0]=0
		arr=[]
		for i in range(1,numMarks):
			arr.append(angleResult[i]-angleResult[i-1])
		print(min(arr),max(arr))
		#print(angleResult)

#FindBeats()
findAngle()
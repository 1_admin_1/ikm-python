import math
import pylab as plt
import numpy as np
import sys
import tkinter as Tk
import json
import time
import random
realX=0
realY=0
radius=215/2
angleSensors=[45.0,45.0]
numMarks=16
resolution=1
beatsX=0
beatsY=0
timeStop=[0.001,0.01]
workFlag=True
#timeStop=[0.1,1]
numRev=3
rpmconst=5000
rpmvar=0
freq_rpmvar=0
freq_adc=500000
res_var=0.001
tableAngle=[[]for x in range(3)]
tableTime=[[]for x in range(3)]
numSync=16

import __main__
points=None
M_RAD=toRad=math.pi/180
M_ANG=toAng=1/toRad
M_PI=math.pi
#angleMarks=[x/numMarks*360+(random.random()/10) for x in range(numMarks)]
angleMarks=[x/numMarks*360 for x in range(numMarks)]
#angleMarks[1]=22.7
timeMarks=[[],[],[]]
aMarks=[[],[],[]]
root=Tk.Tk()

def stop():
	global workFlag
	workFlag=False
def destroy(e):
	global plt,root
	stop()
	plt.close()

root.bind("<Destroy>", destroy)
frame=Tk.Frame(root)
frame.pack()
frame2=Tk.Frame(root)
frame2.pack()
frame3=Tk.Frame(root)
frame3.pack()
frameAngle=None


def labelTable(tbl):
	for i in range(3):
		Tk.Label(tbl, text="Датчик %i"%(i+1)).grid(row=0,column=i)

class TkField:
	num=0
	arr=[]
	def __init__(self,var,txt,*x):
		lebel = Tk.Label(frame, text=txt)
		lebel.grid(row=TkField.num, column=0)
		self.entry = Tk.Entry(frame,  width=10)
		self.entry.grid(row=TkField.num, column=1)
		self.var=var
		TkField.num+=1
		TkField.arr.append(self)
	def update(self):
		v=__main__.__dict__[self.var]
		if type(v)==int:
			__main__.__dict__[self.var]=int(self.entry.get())
		if type(v)==float:
			__main__.__dict__[self.var]=float(self.entry.get())
		if type(v)==list:
			__main__.__dict__[self.var]=json.loads(self.entry.get())
	def updateAll():
		for i in TkField.arr:
			i.update()
	def updateFieldAll():
		for i in TkField.arr:
			i.entry.insert(0,__main__.__dict__[i.var])

def viewTableAngle():
	global frameAngle
	if frameAngle==None: 
		frameAngle=Tk.Tk()
	try:
		labelTable(frameAngle)
	except:
		frameAngle=Tk.Tk()
		labelTable(frameAngle)
	frameAngle.wm_title("Таблица углов")
	for e,sensor in enumerate(tableAngle):
		for i,s in enumerate(sensor):
			Tk.Label(frameAngle, text=str(s)).grid(row=i+1,column=e)


def viewTableTime():
	global tableTime
	TkField.updateAll()
	frameTime=Tk.Tk()
	labelTable(frameTime)
	frameTime.wm_title("Таблица времени")
	tableTime=[[]for x in range(3)]
	if rpmvar==0:
		koef=freq_adc*60/(rpmconst*360)
		for e,i in enumerate(tableAngle):
			for j in i:
				tableTime[e].append(j*koef)
				Tk.Label(frameTime, text=str(j*koef)).grid(row=len(tableTime[e])+1,column=e)
	else:
		angle=0
		alltime=0
		koef=(360)/(freq_adc*60)
		angleOld=0
		indexTime=0
		try:
			maxAngle=max(tableAngle[1][-1],tableAngle[2][-1],tableAngle[0][-1])
		except:
			maxAngle=0
		while True:
			rev=(rpmconst+rpmvar*math.sin((freq_rpmvar/freq_adc*2*math.pi*indexTime)))
			angleOld=angle
			angle+=rev*koef*res_var
			for e,sensor in enumerate(tableAngle):
				for s in sensor:
					if(angleOld<s<angle):
						time=indexTime
						tableTime[e].append(time)
						Tk.Label(frameTime, text=str(time)).grid(row=len(tableTime[e])+1,column=e)
						frameTime.update()
			indexTime+=res_var
			if angle > maxAngle: break


def findAnglePres(i,j,x,y,ox,oy,f,numSensor):
	global frameAngle
	step=1
	if oy<f(ox) and y>=f(x):
		if resolution<1: 
			print(numSensor,i)
			timeMarks[numSensor].append(16000/360*i)
			aMarks[numSensor].append(i)
			return i
		else:
			while(step>0.000000000001):
				step=step/10
				mX=[realX+beatsX*math.cos((o)*toRad)+radius*math.cos((-angleMarks[j]+o)*toRad) for o in np.arange(i-step*10,i+step,step)]
				mY=[realY+beatsY*math.sin((o)*toRad)+radius*math.sin((-angleMarks[j]+o)*toRad) for o in np.arange(i-step*10,i+step,step)]
				for ii in range(1,11):
					if mY[ii-1]<=f(mX[ii-1]) and mY[ii]>f(mX[ii]):
						i=i+ii*step-step*10
			print(numSensor,i,beatsX*math.cos((i)*toRad),beatsY*math.sin((i)*toRad))
			if frameAngle:
				try:
					Tk.Label(frameAngle, text=str(i)).grid(row=len(tableAngle[numSensor])+1,column=numSensor)
				except:
					frameAngle=None
			tableAngle[numSensor].append(i)
			timeMarks[numSensor].append(16000/360*i)
			aMarks[numSensor].append(i)
			points.set_data((realX+bX+radius*math.cos((-angleMarks[j]+i)*toRad),realY+bY+radius*math.sin((-angleMarks[j]+i)*toRad)))
			plt.pause(timeStop[1])
	return i

def restart_line():
    sys.stdout.write('\r')
    sys.stdout.flush()

def start():
	TkField.updateAll()
	global points,bX,bY,workFlag, tableAngle
	tableAngle=[[]for x in range(3)]
	workFlag=True
	fig, ax = plt.subplots()
	rollX=[realX+radius*math.cos(p*toRad) for p in range(360)]
	rollY=[realY+radius*math.sin(p*toRad) for p in range(360)]
	marksX=[realX+radius*math.cos(p*toRad) for p in angleMarks]
	marksY=[realY+radius*math.sin(p*toRad) for p in angleMarks]
	sensorsX=list(range(0,50))
	sensorsY=[]
	sensorsY.append([math.tan(-angleSensors[0]*toRad)*x for x in sensorsX])
	sensorsY.append([0 for x in sensorsX])
	sensorsY.append([math.tan(angleSensors[1]*toRad)*x for x in sensorsX])
	plt.plot(rollX,rollY)
	plt.plot(sensorsX,sensorsY[0])
	plt.plot(sensorsX,sensorsY[1])
	plt.plot(sensorsX,sensorsY[2])
	points, = ax.plot(rollX, rollY, marker='o', linestyle='None')
	points2, = ax.plot(rollX, rollY, marker='o', linestyle='None')
	points3, = ax.plot(rollX, rollY)
	plt.plot(realX,realY,marker='o')
	plt.plot(0,0,marker='o')
	ax.set_xlim(-50, 50) 
	ax.set_ylim(-50, 50)
	marksXOld=[]
	marksYOld=[]
	for i in (r*resolution for r in range(0,int(360*numRev/resolution))):
		bX=beatsX*math.cos((i)*toRad)
		bY=beatsY*math.sin((i)*toRad)
		marksX=[realX+bX+radius*math.cos((-p+i)*toRad) for p in angleMarks]
		marksY=[realY+bY+radius*math.sin((-p+i)*toRad) for p in angleMarks]
		if resolution==1:
			points.set_data(marksX,marksY)
			points2.set_data(realX+bX,realY+bY)
			points3.set_data([x+bX for x in rollX],[y+bY for y in rollY])
		#if int(i)==i: print(i)
		if i>0:
			for j,(x,y,ox,oy) in enumerate(zip(marksX,marksY,marksXOld,marksYOld)):
				f0=(lambda xx: math.tan(-angleSensors[0]*toRad)*xx)
				f1=(lambda xx: 0)
				f2=(lambda xx: math.tan(angleSensors[1]*toRad)*xx)
				try:
					res=findAnglePres(i,j,x,y,ox,oy,f0,0)
					res2=findAnglePres(i,j,x,y,ox,oy,f1,1)
					res3=findAnglePres(i,j,x,y,ox,oy,f2,2)
				except:
					break
		marksXOld=marksX
		marksYOld=marksY
		if resolution==1:
			plt.pause(timeStop[0])
		if not workFlag: break
	#for i in range(0,10):
	#	print(aMarks[2][i+1]-aMarks[1][i])

def syncExtra(i1,i2):
	global timeMarks
	ch1sum1=sum(tableTime[i1][1:numSync+1])
	ch2sum0=sum(tableTime[i2][0:numSync])
	ch2sum1=sum(tableTime[i2][1:numSync+1])
	ch2sum2=sum(tableTime[i2][2:numSync+2])
	if abs(ch1sum1-ch2sum2)<abs(ch1sum1-ch2sum1):
		tableTime[i2].pop(0)
	if abs(ch1sum1-ch2sum0)<abs(ch1sum1-ch2sum1):
		tableTime[i1].pop(0)

def sync():
	syncExtra(0,1)
	syncExtra(1,2)
	syncExtra(0,2)

def timeMarksFileWrite():
	viewTableAngle()
	viewTableTime()
	with open("timeMarksFile.py",'w') as f:
		tmp=sys.stdout
		sys.stdout=f
		sync()
		print("timeMarks=",end='')
		print(tableTime)
		sys.stdout=tmp



TkField('radius','Радиус вала [мм]:')
TkField('numMarks','Количество меток [шт]:')
TkField('realX','Смещение вала по X [мм]:')
TkField('realY','Смещение вала по Y [мм]:')
TkField('beatsX','Биения вала по X [мм]:')
TkField('beatsY','Биения вала по Y [мм]:')

TkField('angleSensors','Углы датчиков [град.]:')
TkField('timeStop','Время анимации [с]:')
TkField('numRev','Количество оборотов [шт]:')
TkField('rpmconst','Обороты (постоянная составляющая) [об/мин]')
TkField('rpmvar','Обороты (переменная составляющая) [об/мин]')
TkField('freq_rpmvar','Частота переменной составляющей [Гц]')
TkField('freq_adc','Частота АЦП [Гц]')
TkField('res_var',"Точность измерения времени")
Tk.Button(frame2,text="Старт",command=start).grid(row=0,column=0)
Tk.Button(frame2,text="Обновить данные",command=TkField.updateAll).grid(row=0,column=1)
Tk.Button(frame2,text="Стоп",command=stop).grid(row=0,column=2)
Tk.Button(frame2,text="Таблица углов",command=viewTableAngle).grid(row=0,column=3)
Tk.Button(frame2,text="Таблица времени",command=viewTableTime).grid(row=0,column=4)
Tk.Button(frame3,text="Вывод углов в консоль",command=(lambda:print(tableAngle))).grid(row=0,column=0)
Tk.Button(frame3,text="Вывод времени в консоль",command=(lambda:print(tableTime))).grid(row=0,column=1)
Tk.Button(frame3,text="Вывод времени в файл",command=timeMarksFileWrite).grid(row=0,column=2)
TkField.updateFieldAll()

if root:
	root.mainloop()
from tkinter import *
from tkinter.filedialog import askopenfilenames
from tkinter.messagebox import showerror
import matplotlib.pyplot as plt
import numpy as np
import datetime
import os

s_freq=500000
s_channel=6
s_filebegin=9

root=Tk()


class OpenFileFrame(Frame):
	"""
	Splitter for large binary ADC file into small chunks.
	"""
	def __init__(self,parent=None,text="Processing File"):
		Frame.__init__(self,parent)
		w = Label(self, text="Spliter ADC file\nEnter chunk size (in seconds):")
		w.pack()
		self.sepEntry=Entry(self)
		self.sepEntry.pack()
		self.button = Button(self, text=text, command=self.load_file, width=16)
		self.button.pack()
		self.parent=parent
	def load_file(self):
		for i in self.load_file_iter():
			pass
	def load_file_iter(self):
		filenames=askopenfilenames(filetypes=(("bin data files", "*.bin"),))
		if type(filenames)==str:
			filenames=filenames.split(".bin ")
			filenames=[i if i[-4:]=='.bin' else i+'.bin' for i in filenames]
		print(filenames)
		for fn in filenames:
			if fn:
				self.read_file(fn)
				self.name_file=fn.split("/")[-1].replace('.bin','')
				yield None

	def read_file(self,fname):
		format = '%Y-%m-%d_%H-%M-%S'
		fdir=os.path.dirname(fname)
		fn=os.path.basename(fname)[:-4]
		self.time=datetime.datetime.strptime(fn, format)
		tm=int(self.sepEntry.get())
		chunksize=s_freq*s_channel*2*tm
		with open(fname, "rb") as f:
			info=f.read(s_filebegin)
			while True:
				chunk = f.read(chunksize)
				if chunk:
					fwrite=open(fdir+'/chunk'+datetime.datetime.strftime(self.time,format)+'.bin','wb')
					fwrite.write(info)
					fwrite.write(chunk)
					fwrite.close()
					self.time+=datetime.timedelta(seconds=tm)
				else:
					break



openfile=OpenFileFrame(root)
openfile.pack()
root.mainloop()
from ikmdata import *
from multiprocessing import Pool,cpu_count,Array
import matplotlib.ticker as ticker
import datetime, os,sys, subprocess,signal

#matplotlib.rcParams.update({'font.size': 22})
class AngleMarkWindow(MainWindow):
	"""
	Apps preparing signal sensor on stand tests
	Detect start/stop position for ciclic tests (on service channel), 
	split signal on chunk, with different position sensor, and noise reduction. 
	"""
	def __init__(self):
		MainWindow.__init__(self)
		Button(self,text="Load Data To Module",command=lambda:self.loadDataSensor()).grid(columnspan=2)
		Button(self,text="Find Range",command=lambda:self.ds.findRange()).grid(columnspan=2)
		Button(self,text="Save Range",command=lambda:self.ds.saveRange()).grid(columnspan=2)
		Button(self,text="Filter and Save Signal",command=lambda:self.ds.filterAndSave()).grid(columnspan=2)
	def loadDataSensor(self):
		self.ds=DataSensor(self.data.channeldata,self.openfile.fname);


class SharedData():
	def __init__(self,data):
		global shareddata
		if "shareddata" not in globals():
			shareddata=[]
		self.numShared=len(shareddata)
		shareddata.append(data)
		self.qq=os.getpid()

	def __call__(self):
		global shareddata
		return shareddata[self.numShared]
	def __del__(self):
		if self.qq==os.getpid():
			shareddata[self.numShared]=[]

class DataSensor():
	def __init__(self,data,fname):
		self.time=SharedData(data[2])
		self.data=SharedData(data[1])
		self.result=[]
		self.fname=fname[:-4]+"-filterMark.bin"


	def findRange(self):
		"""Функция для нахождения точек перехода из рабочего режима и обратно"""
		p=PoolProcessing(self.workerFindRange).calcAutoPool((0,len(self.time())),10000)
		self.result=self.ignoreImpulseNoise(p.getResultFlat())
		plt.figure()
		plt.plot([j-i for i,j in zip(self.result,self.result[1:])])
		plt.show()

	def filterAndSave(self):
		if not self.result:
			f=open('savedRange.bin','rb')
			self.result=np.load(f)
		filterRange=[]
		for i in range(int(len(self.result)/4)):
			filterRange.append([self.result[4*i],self.result[4*i+1]])
		#self.calcCorrectCoef(x) 
		#poly=np.poly1d(self.polyCoef)
		p=PoolProcessing(self.workerFilterSignal).calcPool(filterRange)
		f=open(self.fname,'wb')
		np.save(f,p.getResult())
		f.close()



	def saveRange(self):
		f = open('savedRange2.bin','wb')
		np.save(f, self.result)
		f.close()

	def workerFindRange(self,diapason):
		"""Возвращает все точки, в которых сигнал изменился на 1000 пунктов"""
		start,end=diapason
		result=[]
		for e,(i,j) in enumerate(zip(self.time()[start:end],self.time()[start+1:end])):
			if abs(j-i)>1000:
				result.append(e+start)
		return result

	def workerFilterSignal(self,args):
		"""Возвращает сигнал, обработанный спектральным фильтром, и усредняет 100 точек в одну"""
		start,end = args
		#data=self.adjustLevel(start,end)
		filterFFT=FilterFFT(2**18,20)
		filterFFT.setData(self.data()[start:end])
		result = filterFFT.getAll()
		result2=np.zeros(int(len(result)/100-1),dtype=np.dtype(np.float64))
		for i in range(int(len(result)/100)-1):
			result2[i]=np.sum(result[i*100:(i+1)*100])/100
		return result2

	def ignoreImpulseNoise(self, point):
		"""
		Возвращает точки переходов, после отсеивания испульсных помех, 
		с помощью соседних точек и направления перехода
		"""
		upDown=3
		result=[]
		for i in point:
			left=sum(self.time()[i-1000:i])
			right=sum(self.time()[i+1:i+1001])
			if (left>right and right/left<0.02) or (left<right and left/right<0.02):
				current = 1 if left>right else 0
				if upDown==current:
					del result[-1]
				upDown=current
				print([left,right],i,len(result))
				result.append(i)
		return result

	def calcCorrectCoef(self,x):
		x=x[::4]
		y=[]
		for i in range(len(x)):
			y.append(self.data()[x[i]])
		self.polyCoef=np.polyfit(x,y,2)

	def adjustLevel(self,start,end):
		dataCalc=self.data()[start:end]
		poly=np.poly1d(self.polyCoef)
		sizeChunk=len(dataCalc)
		data=np.zeros(sizeChunk,dtype=np.dtype(np.int16))
		for i in range(sizeChunk):
			data[i]=dataCalc[i]-poly(i+start)
		return data


	

class FilterFFT():
	"""Спектральный фильтр"""
	def __init__(self,size,crop):
		"""size - размер окна, crop - положение точки, с которой обрезается спектр"""
		self.size=size
		self.crop=crop
		self.window=np.hanning(size)
		self.zeroSpectrum=np.zeros(size,dtype=np.dtype(np.complex128))
		self.tempData=[np.zeros(size,dtype=np.dtype(np.float64)),np.zeros(size,dtype=np.dtype(np.float64))]
		self.pingPong=0
		self.point=0
	def setData(self,data):
		self.data=data
		self.dataSize=int(len(data)/self.size)*self.size
	def __iter__(self):
		return self
	def __next__(self):
		"""
		Итерационная функция, преобразующая сигнал из временной в спектральную область,
		вырезающая часть спектра, и преобразует сигнал обратно во временную область.
		Используется оконная функция Ханна, для уменьшения влияний конечности сигнала
		Используемое перекрытие окон - 1/2.

		Возвращает отфильтрованный сигнал, размером с половину окна
		Первая итерация происходит без перекрытия окна, и амплитуда окна имеет вид окна.
		"""
		endChunk=self.point+self.size
		if endChunk>self.dataSize:
			raise StopIteration
		signal=self.data[self.point:self.point+self.size]
		signal=signal*self.window
		spectrum=np.fft.fft(signal)
		self.zeroSpectrum[:self.crop]=spectrum[:self.crop]
		self.zeroSpectrum[-self.crop:]=spectrum[-self.crop:]
		self.tempData[self.pingPong]=np.fft.ifft(self.zeroSpectrum)
		pongPing=0 if self.pingPong else 1
		result=self.tempData[self.pingPong][:self.size/2]+self.tempData[pongPing][self.size/2:]
		self.point+=int(self.size/2)
		self.pingPong=pongPing
		return result
	def getAll(self):
		"""Возвращает весь сигнал, автоматически проводя итерации"""
		arr=np.zeros(self.dataSize-self.size,dtype=np.dtype(np.float64))
		print(",main",self.dataSize-self.size)
		for e,i in enumerate(self):
			if e>0:
				e=e-1
				arr[self.size/2*e:self.size/2*(e+1)]=np.real(i)
		return arr

class PoolProcessing():
	def __init__(self, function):
		self.func=function
		self.cpu=cpu_count()
	def calcPool(self,args):
		self.pool = Pool()
		self.results = self.pool.map(self.func, args)
		self.pool.close()
		self.pool.join()
		return self
	def calcAutoPool(self,diapason,overlap):
		start,end=diapason
		size=end-start
		lap=int(size/self.cpu)
		tmp=[start+lap*i for i in range(self.cpu)]
		laps=[[i,j+overlap]for i,j in zip(tmp,tmp[1:])]
		laps.append([tmp[-1],end])
		self.calcPool(laps)
		return self
	def getResult(self):
		return self.results
	def getResultFlat(self):
		self.answer=self.results[0]
		for r in self.results[1:]:
			try:
				index=r.index(self.answer[-1])+1
			except:
				index=0;
			self.answer.extend(r[index:])
		return self.answer

def checkRunning():
	pid = str(os.getpid())
	import subprocess
	p = subprocess.Popen(['pgrep', '-l' , 'Python'], stdout=subprocess.PIPE)
	out, err = p.communicate()

	for line in out.splitlines():        
	    line = bytes.decode(line)
	    pid = int(line.split(None, 1)[0])
	    print(pid)
	    os.kill(pid, signal.SIGKILL)
	#print(result)
def savePNG(A4=False):
	import os.path
	for i in range(1000):
		name="chart%03d"%i
		if not os.path.isfile(name+'.png'):
			print(name)
			break
	fig = plt.gcf()
	ax=plt.gca()
	target=[[5,3,''],[3,3,'-colon'],[6,4,'-big']]
	maximum=-10000
	minimum=10000

	if A4:
		target.append([11,7,'-A4'])
	for w,h,ext in target:
		fig.set_size_inches(w,h)
		for line in ax.get_lines():
			yd = line.get_ydata()
			maximum=max(yd) if max(yd)>maximum else maximum
			minimum=min(yd) if min(yd)<minimum else minimum
		crop1=(maximum-minimum)*0.05+maximum
		crop2=minimum-(maximum-minimum)*0.05
		ax.set_ylim([crop2,crop1])
		fig.savefig(name+ext+'.png',dpi=300, bbox_inches='tight')
print(__name__)
if __name__=="__main__":
	checkRunning()
	changeSetting({"memoryAllFile": False,'s_freq':100000})
	view(AngleMarkWindow)



